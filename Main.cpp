#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include "Circle.h"
#include "FillColor.h"
#include "Ellipse.h"

using namespace std;

const int WIDTH  = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**){
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL){
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
    //DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

    SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL){
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

    SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

    //YOU CAN INSERT CODE FOR TESTING HERE
	bool isCurve3=0, DrawingCurve=0;
	SDL_SetRenderDrawColor(ren, 255, 255, 0, 255);
	Vector2D p1(100,500), p2(400, 600), p3(400, 250), p4(700, 300);
	SDL_Color Red;
	Red.a = 255, Red.r = 255, Red.g = 255, Red.b = 0;
	//-------------------------------------------------------------------
	//Test for DrawCurve2
	/*DrawCurve2(ren, p1, p2, p3);
	isCurve3 = 0;
	DrawingCurve = 1;*/
	/*DrawCurve3(ren, p1, p2, p3, p4);
	isCurve3 = 1;
	DrawingCurve = 1;*/
	//--------------------------------------------------------------------
	//TEST FOR TRIANGLEFILL
	//TriangleFill(p1, p2, p3, ren, Red);
	//--------------------------------------------------------------------
	//TEST FOR CIRCLE VS CIRCLE
	BresenhamDrawCircle(300, 300, 100, ren);
	BresenhamDrawCircle(500, 500, 200, ren);
	FillIntersectionTwoCircles(300, 300, 100, 500, 500, 200, ren, Red);
	//--------------------------------
	//TEST FOR ELLIPSE VS CIRCLE
	/*BresenhamDrawCircle(300, 300, 100, ren);
	BresenhamDrawEllipse(200, 400, 100, 200, ren);
	FillIntersectionEllipseCircle(200, 400, 100, 200, 300, 300, 100, ren, Red);*/
	//------------------------------------------------------------------------
	//TEST FOR CIRCLE FILL
	//BresenhamDrawCircle(300, 300, 100, ren);
	//CircleFill(300, 300, 100, ren, Red);
	//TEST FOR CIRCLE VS RECTANGLE
	//FillIntersectionRectangleCircle(p3, p4, 500, 500, 300, ren, Red);
	

	





    SDL_RenderPresent(ren);
    //Take a quick break after all that hard work
    //Quit if happen QUIT event
	Vector2D p;
	int test;
	bool running = true;
	while (running)
	{
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{

			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
				
			}
			else
			{
				if (DrawingCurve == 1)
				{
					if (event.type == SDL_MOUSEBUTTONDOWN)
					{
						int x, y;
						SDL_GetMouseState(&x, &y);
						if (isCurve3 == 1)
						{
							if ((x - p1.x)*(x - p1.x) + (y - p1.y)*(y - p1.y) <= 20 * 20) p = p1;else
								if ((x - p2.x)*(x - p2.x) + (y - p2.y)*(y - p2.y) <= 20 * 20) p = p2;else
									if ((x - p3.x)*(x - p3.x) + (y - p3.y)*(y - p3.y) <= 20 * 20) p = p3;else
										if ((x - p4.x)*(x - p4.x) + (y - p4.y)*(y - p4.y) <= 20 * 20) p = p4;
						}
						else
						{
							if ((x - p1.x)*(x - p1.x) + (y - p1.y)*(y - p1.y) <= 20 * 20) p = p1;else
								if ((x - p2.x)*(x - p2.x) + (y - p2.y)*(y - p2.y) <= 20 * 20) p = p2;else
									if ((x - p3.x)*(x - p3.x) + (y - p3.y)*(y - p3.y) <= 20 * 20) p = p3;
						}
					}
					if (event.type == SDL_MOUSEMOTION)
					{
						int x, y;
						SDL_GetMouseState(&x, &y);
						if (isCurve3 == 1)
						{
							if (p.x == p1.x && p.y == p1.y) { p1.x = x, p1.y = y, p.x = x, p.y = y; }
							else
								if (p.x == p2.x && p.y == p2.y) { p2.x = x, p2.y = y, p.x = x, p.y = y; }
								else
									if (p.x == p3.x && p.y == p3.y) { p3.x = x, p3.y = y, p.x = x, p.y = y; }
									else
										if (p.x == p4.x && p.y == p4.y) { p4.x = x, p4.y = y, p.x = x, p.y = y; }
						}
						else
						{
							if (p.x == p1.x && p.y == p1.y) { p1.x = x, p1.y = y, p.x = x, p.y = y; }
							else
								if (p.x == p2.x && p.y == p2.y) { p2.x = x, p2.y = y, p.x = x, p.y = y; }
								else
									if (p.x == p3.x && p.y == p3.y) { p3.x = x, p3.y = y, p.x = x, p.y = y; }
						}
					}
					SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
					SDL_RenderClear(ren);
					SDL_SetRenderDrawColor(ren, 255, 255, 0, 255);
					if (isCurve3)
						DrawCurve3(ren, p1, p2, p3, p4);
					else
						DrawCurve2(ren, p1, p2, p3);
					SDL_RenderPresent(ren);
					if (event.type == SDL_MOUSEBUTTONUP)
					{
						p = NULL;
					}
				}
			}
		}
	}
	
    

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}

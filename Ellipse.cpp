#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren,xc + x, yc + y);
	SDL_RenderDrawPoint(ren,xc - x, yc + y);
	SDL_RenderDrawPoint(ren,xc - x, yc - y);
	SDL_RenderDrawPoint(ren,xc + x, yc - y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    // Area 1
	int x = 0, y = b;
	int p = -2 * a*a*b + a * a + 2 * b*b;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a*a + b * b) <= a * a*a*a)
	{
		if (p > 0)
		{
			p = p + 4 * b*b*x - 4 * a*a*y + 6*b*b + 4*a*a;
			y = y - 1;
		}
		else
		{
			p = p + 4 * b*b*x + 6 * b*b;
		}
		x = x + 1;
		Draw4Points(xc, yc, x, y, ren);
	}
    // Area 2
	x = a, y = 0 ;
	 p = 2 * a*a - 2 * a*b*b + b * b;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a*a + b * b) >= a * a*a*a)
	{
		if (p > 0)
		{
			p = p - 4 * b*b*x + 4 * a*a*y+ 6*a*a + 4* b*b;
			x = x -  1;
		}
		else
		{
			p = p + 4 * a*a*y + 6 * a*a;
		}
		y = y + 1;
		Draw4Points(xc, yc, x, y, ren);
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    // Area 1
	int x = 0;
	int y = b;
	int p = (a*a) - 4 * a*a*b + 4 * b*b;
	Draw4Points(xc, yc, x, x, ren);
	while ((a*a + b * b)*x*x <= a * a*a*a) 
	{ 
		if (p<0)p += (8 * b*b*x + 12 * b*b); 
		else 
		{
			p += (8 * b*b*x - 8 * a*a*y + 8 * a*a + 12 * b*b);
			y--; 
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
	while (y >= 0)
	{
		if (p < 0) {
			p += (8 * b*b*x - 8 * a*a*y + 12 * a*a + 8 * b*b);x++;
		}
		else
		{
			p += (-8 * a*a*y + 12 * a*a);
		}
		y--;
		Draw4Points(xc, yc, x, y, ren);

	}

}
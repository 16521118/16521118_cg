#include "Bezier.h"
#include <iostream>
#include "Circle.h"
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	float x, y;
	for (float t = 0;t <= 1;t = t + 0.001)
	{
		x = (1 - t)*(1 - t)*p1.x + 2 * t*(1 - t)*p2.x + t * t*p3.x;
		y = (1 - t) *(1 - t) * p1.y + 2 * t*(1 - t)*p2.y + t * t * p3.y;
		SDL_RenderDrawPoint(ren, x, y);
	}
	BresenhamDrawCircle(p1.x, p1.y, 5, ren);
	BresenhamDrawCircle(p2.x, p2.y, 5, ren);
	BresenhamDrawCircle(p3.x, p3.y, 5, ren);
}

void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	float x, y;
	for (float t = 0; t <= 1;t = t + 0.001)
	{
		x = (1 - t)*(1 - t)*(1 - t)*p1.x + 3 * t*(1 - t)*(1 - t)*p2.x + 3 * t*t*(1 - t)*p3.x + t * t*t*p4.x;
		y = (1 - t)*(1 - t)*(1 - t)*p1.y + 3 * t*(1 - t)*(1 - t)*p2.y + 3 * t*t*(1 - t)*p3.y + t * t*t*p4.y;
		SDL_RenderDrawPoint(ren, x, y);
	}
	BresenhamDrawCircle(p1.x, p1.y, 5, ren);
	BresenhamDrawCircle(p2.x, p2.y, 5, ren);
	BresenhamDrawCircle(p3.x, p3.y, 5, ren);
	BresenhamDrawCircle(p4.x, p4.y, 5, ren);
}



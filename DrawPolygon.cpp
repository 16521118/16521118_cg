#include "DrawPolygon.h"
#include "Line.h"
#include <iostream>
#define PI 3.14
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3];
	int y[3];
	float gocquay = PI / 2;
	for (int i = 0; i < 3; i++)
	{
		x[i] = xc + int(R*cos(gocquay) + 0.5);
		y[i] = yc - int(R*sin(gocquay) + 0.5);
		gocquay = gocquay + 2 * PI / 3;
	}
	DDA_Line(x[0], y[0], x[1], y[1], ren);
	DDA_Line(x[1], y[1], x[2], y[2], ren);
	DDA_Line(x[0], y[0], x[2], y[2], ren);

	
}
void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4];
	int y[4];
	float gocquay = 0;
	for (int i = 0; i < 4; i++)
	{
		x[i] = xc + int(R*cos(gocquay) + 0.5);
		y[i] = yc - int(R*sin(gocquay) + 0.5);
		gocquay = gocquay + 2 * PI / 4;
	}
	for (int i = 0; i < 4; i++)
		DDA_Line(x[i], y[i], x[(i + 1) % 4], y[(i + 1) % 4], ren);
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5];
	int y[5];
	float gocquay = PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(gocquay) + 0.5);
		y[i] = yc - int(R*sin(gocquay) + 0.5);
		gocquay = gocquay + 2 * PI / 5;
	}
	for (int i = 0; i < 5; i++)
		DDA_Line(x[i], y[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6];
	int y[6];
	float gocquay = PI/2;
	for (int i = 0; i < 6; i++)
	{
		x[i] = xc + int(R*cos(gocquay) + 0.5);
		y[i] = yc - int(R*sin(gocquay) + 0.5);
		gocquay = gocquay + 2 * PI / 6;
	}
	for (int i = 0; i < 6; i++)
		DDA_Line(x[i], y[i], x[(i + 1) % 6], y[(i + 1) % 6], ren);
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5];
	int y[5];
	float gocquay = PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(gocquay) + 0.5);
		y[i] = yc - int(R*sin(gocquay) + 0.5);
		gocquay = gocquay + 2 * PI / 5;
	}
	for (int i = 0; i < 5; i++)
		DDA_Line(x[i], y[i], x[(i + 2) % 5], y[(i + 2) % 5], ren);
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], xp[5];
	int y[5], yp[5];
	float gocquay = PI / 2;
	float r = R * sin(PI / 10) / sin(7 * PI / 10);
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(gocquay) + 0.5);
		y[i] = yc - int(R*sin(gocquay) + 0.5);
		xp[i] = xc + int(r*cos(gocquay + PI / 5) + 0.5);
		yp[i] = yc - int(r*sin(gocquay + PI / 5) + 0.5);
		gocquay += 2 * PI / 5;
	}

	for (int i = 0; i < 5; i++)
	{
		DDA_Line(x[i], y[i], xp[i], yp[i], ren);
		DDA_Line(xp[i], yp[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8], xp[8];
	int y[8], yp[8];
	float gocquay = 0;
	float r = R * sin(PI / 8) / sin(6 * PI / 8);
	for (int i = 0; i < 8; i++)
	{
		x[i] = xc + int(R*cos(gocquay) + 0.5);
		y[i] = yc - int(R*sin(gocquay) + 0.5);
		xp[i] = xc + int(r*cos(gocquay + PI / 8) + 0.5);
		yp[i] = yc - int(r*sin(gocquay + PI / 8) + 0.5);
		gocquay += 2 * PI / 8;
	}

	for (int i = 0; i < 8; i++)
	{
		DDA_Line(x[i], y[i], xp[i], yp[i], ren);
		DDA_Line(xp[i], yp[i], x[(i + 1) % 8], y[(i + 1) % 8], ren);
	}
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int x[5], xp[5];
	int y[5], yp[5];
	float gocquay = startAngle;
	float r = R * sin(PI / 10) / sin(7 * PI / 10);
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(gocquay) + 0.5);
		y[i] = yc - int(R*sin(gocquay) + 0.5);
		xp[i] = xc + int(r*cos(gocquay + PI / 5) + 0.5);
		yp[i] = yc - int(r*sin(gocquay + PI / 5) + 0.5);
		gocquay += 2 * PI / 5;
	}

	for (int i = 0; i < 5; i++)
	{
		DDA_Line(x[i], y[i], xp[i], yp[i], ren);
		DDA_Line(xp[i], yp[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	int bankinh = r;
	float gocquay = PI / 2;
	while (bankinh > 1) {
		DrawStarAngle(xc, yc, int(bankinh + 0.5), gocquay, ren);
		gocquay = gocquay + PI;
		bankinh = bankinh * sin(PI / 10) / sin(7 * PI / 10);
	}
}

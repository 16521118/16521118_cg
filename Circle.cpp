#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int new_x;
	int new_y;

	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	//7 points

	SDL_RenderDrawPoint(ren,xc - x, yc + y);

	SDL_RenderDrawPoint(ren,xc + x, yc - y);

	SDL_RenderDrawPoint(ren,xc - x, yc - y);

	SDL_RenderDrawPoint(ren,xc + y, yc + x);

	SDL_RenderDrawPoint(ren,xc - y, yc + x);

	SDL_RenderDrawPoint(ren,xc + y, yc - x);

	SDL_RenderDrawPoint(ren,xc - y, yc - x);
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0;
	int y = R;
	int p = 3 - 2 * R;
	Draw8Points(xc, yc, x, y, ren);
	while (x <= y)
	{
		Draw8Points(xc, yc, x, y, ren);
		x = x + 1;
		if (p < 0)
		{
			p = p + 4 * x + 6;
		}
		else
		{
			y = y - 1;
			p = p + 4 * (x - y) + 10;
		}
	}
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int P = 1 - R;
	int x =0; int y =  R;
	Draw8Points(xc, yc, x, y, ren);
	while (x < y)
	{
		if (P < 0)
		{
			P = P + 2 * x + 3;
		}
		else
		{
			y = y - 1;
			P = P + 2 * (x - y) + 5;
		}
		x = x + 1;
		Draw8Points(xc, yc, x, y, ren);
	}
}
